+++
title = "WeatherAPI を使ってお外の様子を知る"
date = 2024-01-29T00:00:00+09:00
lastmod = 2024-01-29T17:29:06+09:00
tags = ["API", "Shell", "script", "jq", "weather", "conky"]
draft = false
+++

いまお外何度？ Data is not foun度. <br/>

<!--more-->


## 概要 {#概要}

[WeatherAPI.com](https://www.weatherapi.com/) を使って 体感温度とか湿度を取得したよというお話 <br/>


## 目的 {#目的}

Conky で現在の外気温とかの情報が欲しくなったので解決したい <br/>


## 要件 {#要件}

-   現在の外気温が取れる <br/>
-   30分に1回程度の更新があると嬉しい <br/>
-   お財布に優しい <br/>
-   天気予報は不要 <br/>
-   せっかくなので皆が使ってなさそうなものに手を出す <br/>
-   単位は 温度[°C] 気圧[hPa] 風速[m/s] で欲しい <br/>


## Weather API {#weather-api}

なんやかんやあって [WeatherAPI.com](https://www.weatherapi.com/) の 無料プラン を使ってみようということにした <br/>
使える機能に制限はあるものの、 <br/>
商用/非商用を問わない(リンク貼るとベネ)上に、 <br/>
`1,000,000 call/month` も叩いていいとか <br/>
個人利用では持て余すくらい使わせてくれる <br/>


### データの更新頻度 {#データの更新頻度}

リアルタイム情報は更新が `10-15分間隔` <br/>
天気予報は更新が `4-6時間間隔` らしい <br/>
FAQ を探すのに手間取ったけど [Pricing - WeatherAPI.com](https://www.weatherapi.com/pricing.aspx) の下の方にあった <br/>


### 取得できる現在の天候情報 {#取得できる現在の天候情報}

で、現在の天候情報について、何が取得できるのか言うと <br/>

-   気温 <br/>
-   湿度 <br/>
-   体感温度 <br/>
-   天気 <br/>
-   雲量 <br/>
-   降水量 <br/>
-   風/突風 <br/>
-   気圧 <br/>
-   UV <br/>
-   最終更新時刻 <br/>

といったところ。体感温度があるのがおもしろそう <br/>


## 使ってみよう {#使ってみよう}


### リクエスト方法 {#リクエスト方法}

endpoint は `https://api.weatherapi.com/v1/current.json` で <br/>
`GET` リクエストを投げる <br/>
suffix を切り替えることで json 形式と xml 形式を利用可能 <br/>

必須クエリは `key` と `q` <br/>
追加で指定したクエリは `lang` <br/>
詳細は <https://www.weatherapi.com/docs/> にて <br/>

`key`
: API key <br/>

`q`
: location <br/>
    都市名、IPアドレス、緯度経度などで指定する <br/>

`lang`
: language <br/>
    必須ではないけれど、天気の部分の言語が指定できる <br/>


### レスポンス {#レスポンス}

成功した場合はざっくりこんな形 <br/>

```text
{
  "location" : { ... },
  "current" : {
     ここに欲しい情報がある
  }
}
```

用があるのは `current` の中のみ <br/>


### 方針 {#方針}

-   `curl` で fetch <br/>
-   `jq` で parse <br/>
-   `date` も使って最終更新時刻から近い時にはAPI叩かないようにしよう <br/>
-   出力は `awk` で抽出しやすいと後で便利 <br/>


## できたコード {#できたコード}

get-current-weather.sh <br/>

```bash
#!/bin/bash

TEMPDIR=/tmp

apikey_path=~/.config/get-current-weather/weather-api-key
latlon_path=~/.config/get-current-weather/latlon
apikey=$(cat "$apikey_path")
latlon=$(cat "$latlon_path")

entrypoint="https://api.weatherapi.com/v1/current.json"
query="key=${apikey}&q=${latlon}&lang=ja"
tmp="$TEMPDIR/gcw-current-weather.json"


# km/h → m/s
function kmh2ms() {
    ms=$(echo "$1 * 0.277777" | bc)
    printf "%.1f" $ms
}


# 情報の取得、最短でも25分間隔とする
fetch=yes
if [ -e "$tmp" ]; then
    lupd=$(cat "$tmp" | jq -r '.current.last_updated_epoch')
    nowt=$(( $(date +%s) - 60 * 25 ))
    if [ $lupd -gt $nowt ]; then
        fetch=no
    fi
fi

if [ "$fetch" == "yes" ]; then
    curl -s "${entrypoint}?${query}" -o $tmp
fi


# 取得した情報の出力
lastupdated=$(cat "$tmp" | jq -r '.current.last_updated')
condition=$(cat "$tmp" | jq -r '.current.condition.text')
temperature=$(cat "$tmp" | jq -r '.current.temp_c')
feelslike=$(cat "$tmp" | jq -r '.current.feelslike_c')
humidity=$(cat "$tmp" | jq -r '.current.humidity')
pressure=$(cat "$tmp" | jq -r '.current.pressure_mb')
wind=$(kmh2ms $(cat "$tmp" | jq -r '.current.wind_kph'))
wind_dir=$(cat "$tmp" | jq -r '.current.wind_dir')
wind_degree=$(cat "$tmp" | jq -r '.current.wind_degree')
gust=$(kmh2ms $(cat "$tmp" | jq -r '.current.gust_kph' ))
echo -e "Updated at ${lastupdated}"
echo -e "Weather:\t\t${condition}"
echo -e "Temperature:\t\t${temperature}\t°C"
echo -e "Feelslike:\t\t${feelslike}\t°C"
echo -e "Humidity:\t\t${humidity}\t%"
echo -e "Pressure:\t\t${pressure}\thPa"
echo -e "Wind:\t\t\t${wind}\tm/s"
echo -e "Wind_direction:\t\t${wind_dir}"
echo -e "Wind_degree:\t\t${wind_degree}\t°"
echo -e "Gust:\t\t\t${gust}\tm/s"
echo -e "\nData source:\t\tWeartherAPI\thttps://www.weatherapi.com"
```

`~/.config/get-current-weather/weather-api-key` <br/>
`~/.config/get-current-weather/latlon` <br/>
sensitive data はハードコーディングしたくないので <br/>
この2つのファイルで外からパラメータを与えるようにした <br/>

latlon ファイルは中身を `35.000,135.000` のようにして <br/>
緯度経度で使っているけれど、 <br/>
特に弄らずそのまま `q=` に渡してるので <br/>
都市名とか入れてもいけるはず <br/>


## 出力例 {#出力例}

```text
> ./get-current-weather.sh
Updated at 2024-01-29 15:30
Weather:                所により曇り
Temperature:            10.0    °C
Feelslike:              9.4     °C
Humidity:               43      %
Pressure:               1026.0  hPa
Wind:                   3.6     m/s
Wind_direction:         SSE
Wind_degree:            150     °
Gust:                   5.6     m/s

Data source:            WeartherAPI     https://www.weatherapi.com
```

いい感じ <br/>

あとは conky で <br/>
`${texeci 1200 get-current-weather.sh | grep Temperature | awk '{print $2 $3}'}` <br/>
などとするだけなので割愛 <br/>


## 感想 {#感想}

-   欲しいものはつくれた <br/>
-   APIがシンプルで利用が簡単だった <br/>
-   体感温度が取れるのはおもしろい <br/>
-   tempearture の綴りを間違えまくった <br/>

