+++
title = "Hugo導入とか"
date = 2024-01-28
lastmod = 2024-01-28T20:10:52+09:00
tags = ["hugo", "org", "GitLab", "Pages"]
draft = false
+++

blog = Hugo + org-mode + ox-hugo + GitLab Pages <br/>

<!--more-->


## 目的 {#目的}

org-mode で記事書いて公開したい <br/>
hugo 使ってみたい <br/>


## 手順 {#手順}


### Hugo を用意する {#hugo-を用意する}

お好みの方法で [Hugo](https://gohugo.io/installation/) をインストールする <br/>
今回はディストリから入れた <br/>

```sh
emerge www-apps/hugo
```


### project を用意する {#project-を用意する}

```sh
hugo new site <dirname>
cd <dirname>
git init
```


### theme を入れる {#theme-を入れる}

[Hugo Themes](https://themes.gohugo.io/) で好きなのを選ぶか、自前で用意する <br/>

```sh
git submodule add https://github.com/kaiiiz/hugo-theme-monochrome.git themes/hugo-theme-monochrome
```

[Monochrome](https://themes.gohugo.io/themes/hugo-theme-monochrome/) がよさげに見えたので使ってみる <br/>


### いくつか設定をする {#いくつか設定をする}

&lt;prj.rt.&gt;/hugo.yaml <br/>

```yaml
theme: hugo-theme-monochrome
title: Site Title
baseURL: https://example.com
```

最低限これ <br/>

追加でこれ <br/>

```yaml
markup:
  goldmark:
    renderer:
      unsafe: true
```

こっちは、 `#+OPTIONS: \n:t` みたいなことするために、 <br/>
`ox-hugo` に `<br/>` を付けて出力してもらうので、 <br/>
その html tag を消されないようにする手 <br/>


#### 参照 {#参照}

[Hugo のショートコード内で Markdown を使う](https://memorandom.whitepenguins.com/posts/hugo-shortcode/)  <br/>


### [ox-hugo](https://ox-hugo.scripter.co/) を入れる {#ox-hugo-を入れる}

init.el <br/>

```elisp
(use-package ox-hugo
  :after ox
  :custom (org-export-preserve-breaks t))
```


### 記事を書く {#記事を書く}

[ox-hugo - Org to Hugo exporter](https://ox-hugo.scripter.co/#demo) を参考に書く <br/>

`ox-hugo` で記事を生成する場合、 <br/>

One post per Org file
: 単一のorgファイルから、単一の記事を出力する方法 <br/>

One post per Org subtree
: 単一のorgファイルから、各subtree単位で複数の記事を出力する方法 <br/>

の2種類がある <br/>

今回は orgファイル と 記事 が1:1対応になる方法で書いてる <br/>
具体例は [この記事のソース](https://gitlab.com/YueKaburagi/blog/-/blob/master/src/articles/first-point.org) でも見て <br/>


### ox-hugo で変換する {#ox-hugo-で変換する}

前述の通り One post per Org file なので、 `C-c C-e H h` で出力 <br/>


### 上手くいったか確認 {#上手くいったか確認}

`hugo server --buildDrafts` でlocalhostにサーバーが立つのでそこから見る <br/>


### 下書き指定を外す {#下書き指定を外す}

記事に及第点を出せたら下書き指定を外す <br/>
`#+hugo_draft: false` <br/>


### GitLab Pages で公開する {#gitlab-pages-で公開する}

`.gitlab-ci.yml` を書く <br/>

```yaml
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  HUGO_ENV: production

pages:
  script:
  - hugo --gc --minify
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

で push <br/>


#### 参照 {#参照}

[GitLab Pages examples / hugo · GitLab](https://gitlab.com/pages/hugo) <br/>
[Host on GitLab Pages | Hugo](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) <br/>
[Emacs org-mode + Hugo + GitLab でブログ | Ymd Blog](https://ymd_h.gitlab.io/ymd_blog/posts/orgmode_hugo_gitlab_blog/) <br/>


### 公開されたサイトを確認する {#公開されたサイトを確認する}

公開が上手くいっていた場合、デフォルトでは <br/>
`https://<username>.gitlab.io/<projectname>` で公開される <br/>


#### 参照 {#参照}

[GitLab Pages default domain names and URLs | GitLab](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html) <br/>


### アクセスしたら hash値 が入った URL に リダイレクトされた？ {#アクセスしたら-hash値-が入った-url-に-リダイレクトされた}

しかもそれで 追加リソース の取得に問題が起きた？ <br/>

GitLab &gt; Deplay &gt; Pages で <br/>
Use unique domain のチェックを外す <br/>

