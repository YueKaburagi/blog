+++
title = "Org-mode のリンクから path を貰って exec call したい"
date = 2024-02-16
lastmod = 2024-02-16T23:09:44+09:00
tags = ["emacs", "elisp"]
draft = false
+++

リンクのpathを外部コマンドに渡したいなって <br/>

<!--more-->


## 動機 {#動機}

orgファイルからリンクしてあるpdfを mupdf で開きたい <br/>

```org
[[file:~/hogehoge/fuga.pdf][同人誌1]]
[[file:~/hogehoge/piyo.pdf][同人誌2]]
[[file:~/hogehoge/moge.pdf][同人誌3]]
```

↑こんな感じの箇所があって、 emacs でも開けないことはないんだけど <br/>
pdfにクソデカページがちょいちょい混じってるんで <br/>
emacs であれこれするにはちょっと向いてなかった <br/>

`org-link-set-parameters` でなんとかする方法だと <br/>
既存のリンクの修正と、将来のリンク取得の修正との <br/>
2箇所に手を入れる必要があって面倒じゃった <br/>


## 結果 {#結果}

```elisp
;; `shell-command' が開くバッファ関係の設定
(setq async-shell-command-buffer 'new-buffer)
(add-to-list 'display-buffer-alist (cons shell-command-buffer-name-async
                                         (cons 'display-buffer-no-window
                                               '((allow-no-window . t)))))

(defun my-org/exec-async-by (command)
  "カーソル位置の org link から取れる path を COMMAND に与えて非同期実行する."
  (let* ((ctx (org-element-context))
         (type (org-element-type ctx)))
    (cond
     ((eq type 'link)
      (async-shell-command (format "%s %s" command (org-element-property :path ctx))))
     (t (user-error "No link at point")))))
(defun my-org/exec-async-by-mupdf ()
  "Open org link by mupdf."
  (interactive)
  (my-org/exec-async-by "mupdf"))
```

こんな感じに書いて、 <br/>
`my-org/exec-async-by-mupdf` を適当にバインドして <br/>
link を mupdf で asynchronous に開けるようにした <br/>


## 途中の問題と解決 {#途中の問題と解決}


### exec call はどうする {#exec-call-はどうする}

`shell-command` でよさそう <br/>
与える COMMAND の末尾を `&` にすると async になる <br/>
又は `async-shell-command` を利用して、自動的に `&` を付けてもらう <br/>


### link から情報はどうやってとる？ {#link-から情報はどうやってとる}


#### `org-element-context` なり `org-element-at-point` なり使え {#org-element-context-なり-org-element-at-point-なり使え}

あとは element をこねるだけ <br/>


#### 参考 {#参考}

`org-open-at-point` のソース <br/>
[Org Element API #Link](https://orgmode.org/worg/dev/org-element-api.html#orgfebaded) <br/>


### `shell-command` で非同期実行すると2つ目以降のバッファで確認が入る {#shell-command-で非同期実行すると2つ目以降のバッファで確認が入る}

`async-shell-command-buffer` に `new-buffer` を指定することで <br/>
確認せずに新しいバッファを開かせるようにした <br/>


### そもそも非同期実行でバッファが表に出ないようにするべきでは？ {#そもそも非同期実行でバッファが表に出ないようにするべきでは}


#### `display-buffer-alist` で制御できるらしい {#display-buffer-alist-で制御できるらしい}

要素は (CONDITION . ACTION) の cons cell で <br/>
ACTION はさらに (FUNCTIONS . ALIST) の cons cell <br/>

CONDITION
: バッファ名に対する regexp 又は、 <br/>
    2引数(バッファ名, `display-buffer` から与えられる action 引数)を受けて boolean を返す函数 <br/>
    
    `shell-command` が非同期で開く場合のデフォルトのバッファ名は <br/>
    `shell-command-buffer-name-async` に入ってる <br/>

FUNCTIONS
: `action function` か、そのリスト <br/>

`action function`
: 定義済みの `action function` があるので、その symbol を使えばいい <br/>
    
    今回だと `display-buffer-no-window` が使いたいやつ <br/>

ALIST
: 定義済みの symbol と そのsymbolが表現する項目の値 の組のリスト <br/>
    
    今回だと `(allow-no-window . t)` で <br/>
    バッファが表に出なくてもいいと設定する <br/>


#### 参考 {#参考}

[Buffer Display Action Functions (GNU Emacs Lisp Reference Manual)/日本語訳](https://ayatakesi.github.io/lispref/28.2/html/Buffer-Display-Action-Functions.html) <br/>
[Buffer Display Action Alists (GNU Emacs Lisp Reference Manual)/日本語訳](https://ayatakesi.github.io/lispref/28.2/html/Buffer-Display-Action-Alists.html) <br/>
[How to use "display-buffer-no-window" with "display-buffer-alist" - Emacs Stack Exchange](https://emacs.stackexchange.com/questions/72031/how-to-use-display-buffer-no-window-with-display-buffer-alist) <br/>


## 既知の問題 {#既知の問題}


### `*Async Shell Command*` バッファが開かれっぱなしになる {#async-shell-command-バッファが開かれっぱなしになる}

いくつ開いているかは `list-buffers` でもしないと見えないので <br/>
pdf を開いては閉じ。を繰り返したら沢山バッファが開かれますね？ <br/>

後で困った時に考える <br/>


### 全ての `*Async Shell Command*` バッファが裏で開くようになったので、stdoutに用があるような時に不便 {#全ての-async-shell-command-バッファが裏で開くようになったので-stdoutに用があるような時に不便}

こっちも後で困った時に考える <br/>

`shell-command` にはバッファが渡せるので、それを利用したらいいはず <br/>

