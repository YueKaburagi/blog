#+hugo_base_dir: ../../
#+hugo_section: tech
#+title: Org-mode のリンクから path を貰って exec call したい
#+filetags: :emacs:elisp:
#+startup: overview
#+date: 2024-02-16
#+hugo_auto_set_lastmod: t


#+hugo_draft: false

リンクのpathを外部コマンドに渡したいなって
#+hugo: more
* 動機
orgファイルからリンクしてあるpdfを mupdf で開きたい
#+begin_src org
[[file:~/hogehoge/fuga.pdf][同人誌1]]
[[file:~/hogehoge/piyo.pdf][同人誌2]]
[[file:~/hogehoge/moge.pdf][同人誌3]]
#+end_src
↑こんな感じの箇所があって、 emacs でも開けないことはないんだけど
pdfにクソデカページがちょいちょい混じってるんで
emacs であれこれするにはちょっと向いてなかった

=org-link-set-parameters= でなんとかする方法だと
既存のリンクの修正と、将来のリンク取得の修正との
2箇所に手を入れる必要があって面倒じゃった
* 結果
#+begin_src elisp
;; `shell-command' が開くバッファ関係の設定
(setq async-shell-command-buffer 'new-buffer)
(add-to-list 'display-buffer-alist (cons shell-command-buffer-name-async
                                         (cons 'display-buffer-no-window
                                               '((allow-no-window . t)))))

(defun my-org/exec-async-by (command)
  "カーソル位置の org link から取れる path を COMMAND に与えて非同期実行する."
  (let* ((ctx (org-element-context))
         (type (org-element-type ctx)))
    (cond
     ((eq type 'link)
      (async-shell-command (format "%s %s" command (org-element-property :path ctx))))
     (t (user-error "No link at point")))))
(defun my-org/exec-async-by-mupdf ()
  "Open org link by mupdf."
  (interactive)
  (my-org/exec-async-by "mupdf"))
#+end_src
こんな感じに書いて、
=my-org/exec-async-by-mupdf= を適当にバインドして
link を mupdf で asynchronous に開けるようにした
* 途中の問題と解決
** exec call はどうする
=shell-command= でよさそう
与える COMMAND の末尾を =&= にすると async になる
又は =async-shell-command= を利用して、自動的に =&= を付けてもらう
** link から情報はどうやってとる？
*** =org-element-context= なり =org-element-at-point= なり使え
あとは element をこねるだけ
*** 参考
=org-open-at-point= のソース
[[https://orgmode.org/worg/dev/org-element-api.html#orgfebaded][Org Element API #Link]]
** ~shell-command~ で非同期実行すると2つ目以降のバッファで確認が入る
=async-shell-command-buffer= に =new-buffer= を指定することで
確認せずに新しいバッファを開かせるようにした
** そもそも非同期実行でバッファが表に出ないようにするべきでは？
*** =display-buffer-alist= で制御できるらしい
要素は (CONDITION . ACTION) の cons cell で
ACTION はさらに (FUNCTIONS . ALIST) の cons cell
+ CONDITION ::
  バッファ名に対する regexp 又は、
  2引数(バッファ名, =display-buffer= から与えられる action 引数)を受けて boolean を返す函数
  
  =shell-command= が非同期で開く場合のデフォルトのバッファ名は
  =shell-command-buffer-name-async= に入ってる
+ FUNCTIONS :: 
  =action function= か、そのリスト
+ =action function= :: 
  定義済みの =action function= があるので、その symbol を使えばいい
  
  今回だと =display-buffer-no-window= が使いたいやつ
+ ALIST ::
  定義済みの symbol と そのsymbolが表現する項目の値 の組のリスト
  
  今回だと =(allow-no-window . t)= で
  バッファが表に出なくてもいいと設定する
*** 参考
[[https://ayatakesi.github.io/lispref/28.2/html/Buffer-Display-Action-Functions.html][Buffer Display Action Functions (GNU Emacs Lisp Reference Manual)/日本語訳]]
[[https://ayatakesi.github.io/lispref/28.2/html/Buffer-Display-Action-Alists.html][Buffer Display Action Alists (GNU Emacs Lisp Reference Manual)/日本語訳]]
[[https://emacs.stackexchange.com/questions/72031/how-to-use-display-buffer-no-window-with-display-buffer-alist][How to use "display-buffer-no-window" with "display-buffer-alist" - Emacs Stack Exchange]]
* 既知の問題
** =*Async Shell Command*= バッファが開かれっぱなしになる
いくつ開いているかは =list-buffers= でもしないと見えないので
pdf を開いては閉じ。を繰り返したら沢山バッファが開かれますね？

後で困った時に考える
** 全ての =*Async Shell Command*= バッファが裏で開くようになったので、stdoutに用があるような時に不便
こっちも後で困った時に考える

=shell-command= にはバッファが渡せるので、それを利用したらいいはず

